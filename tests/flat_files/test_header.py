"""Some tests for the header module
"""
from pyaddons import utils
from pyaddons.flat_files import header
import pytest
import gzip
import lzma
import bz2
import os
import csv
import random

_SEED = 1984
"""The random seed for file generation (`int`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_file(filename, skiplines=0, comment_lines=5, comment_char=None,
                has_header=True, ncols=10, nrows=10, delimiter="\t",
                write_method=open):
    """Create a test file to work on header detection.

    Parameters
    ----------
    filename : `str`
        The filename to write to.
    skiplines : `int`
        Write some meaningless lines at the start of the file.
    comment_lines : `int`
        The number of comment lines to write if ``comment_char`` is not
        ``NoneType``.
    comment_char : `str`, optional, default: `NoneType`
        Write 5 comment character lines after any skiplines.
    has_header : `bool`, optional, default: `\t`
        Write a header line.
    ncols : `int`, optional, default: `10`
        The number of columns to write.
    nrows : `int`, optional, default: `10`
        The number of rows to write.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the file.
    is_gzip : `bool`, optional, default: `False`
        Should the output file be gzip compressed.
    """
    with write_method(filename, 'wt') as outfile:
        for i in range(skiplines):
            outfile.write(f"{i}\n")

        if comment_char is not None and comment_lines > 0:
            for i in range(comment_lines):
                outfile.write(f"  {comment_char} {i}\n")

        if has_header is True:
            outfile.write("{0}\n".format(
                delimiter.join([f"column{i}" for i in range(ncols)])
            ))

        data_funcs = [_get_string, _get_float, _get_integer]
        for i in range(nrows):
            row = []
            for j in range(ncols):
                random.seed(_SEED * j * 2)
                df = data_funcs[random.randint(0, len(data_funcs)-1)]
                row.append(df(_SEED * i))
            outfile.write("{0}\n".format(
                delimiter.join([str(r) for r in row])
            ))

    # with write_method(filename, 'rt') as infile:
    #     for row in infile:
    #         print(row.strip())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_string(seed=_SEED):
    """Get a text string for a random integer value between 1-5000 (`str`)
    """
    random.seed(seed)
    return utils.get_text_number(random.randint(1, 5000))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_float(seed=_SEED):
    """Get a random float value between 0-1 (`float`)
    """
    random.seed(seed)
    return random.random()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_integer(seed=_SEED):
    """Get a random integer value between 1-5000 (`int`)
    """
    random.seed(seed)
    return random.randint(1, 5000)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "skiplines,comment_char,has_header,ncols,nrows,delimiter,open_method,sniff_bytes",
    [
        (0, None, True, 7, 10, "\t", gzip.open, 8192),
        (0, None, False, 7, 10, "\t", lzma.open, 2000000),
        (10, "#", True, 7, 10, "\t", bz2.open, 2000000),
        (10, "#", False, 7, 10, "\t", open, 2000000),
        (0, None, True, 7, 10, "|", gzip.open, 8192),
        (0, None, False, 7, 10, "|", lzma.open, 2000000),
        (10, "#", True, 7, 10, "|", bz2.open, 2000000),
        (10, "#", False, 7, 10, "|", open, 2000000),
        (0, None, True, 7, 10, ",", gzip.open, 8192),
        (0, None, False, 7, 10, ",", lzma.open, 8192),
        (10, "#", True, 7, 10, ",", bz2.open, 8192),
        (10, "#", False, 7, 10, ",", open, 8192),
    ]
)
def test_detect_header(tmpdir, skiplines, comment_char, has_header, ncols,
                       nrows, delimiter, open_method, sniff_bytes):
    """Tests for the header detection function
    ``pyaddons.flat_files.header.detect_header``
    """
    test_file_name = utils.get_temp_file(dir=tmpdir)

    try:
        create_file(
            test_file_name, skiplines=skiplines,
            comment_char=comment_char, has_header=has_header,
            ncols=ncols, nrows=nrows, delimiter=delimiter,
            write_method=open_method
        )

        first_row, dialect, res_header, res_skiplines = header.detect_header(
            test_file_name, open_method=open_method, skiplines=skiplines,
            comment=comment_char, delimiters=delimiter,
            sniff_bytes=sniff_bytes
        )

        assert len(first_row) == ncols, "wrong column number"
        assert dialect.delimiter == delimiter, "wrong delimiter"
        assert has_header == res_header, "bad header detection"
        assert (skiplines + (bool(skiplines) * 5)) == res_skiplines
    finally:
        os.unlink(test_file_name)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "skiplines,comment_char,has_header,ncols,nrows,delimiter,open_method",
    [
        (0, None, True, 7, 10, "\t", gzip.open),
        (0, None, True, 7, 10, "\t", lzma.open),
        (10, "#", True, 7, 10, "\t", bz2.open),
        (10, "#", True, 7, 10, "\t", open),
        (0, None, True, 7, 10, "|", gzip.open),
        (0, None, True, 7, 10, "|", lzma.open),
        (10, "#", True, 7, 10, "|", bz2.open),
        (10, "#", True, 7, 10, "|", open),
        (0, None, True, 7, 10, ",", gzip.open),
        (0, None, True, 7, 10, ",", lzma.open),
        (10, "#", True, 7, 10, ",", bz2.open),
        (10, "#", True, 7, 10, ",", open),
    ]
)
def test_get_header_from_file(tmpdir, skiplines, comment_char, has_header,
                              ncols, nrows, delimiter, open_method):
    """Tests for the header detection function
    ``pyaddons.flat_files.header.detect_header``
    """
    test_file_name = utils.get_temp_file(dir=tmpdir)

    try:
        create_file(
            test_file_name, skiplines=skiplines,
            comment_char=comment_char, has_header=has_header,
            ncols=ncols, nrows=nrows, delimiter=delimiter,
            write_method=open_method
        )

        # This is the expected header we are testing against
        exp_header = delimiter.join([f"column{i}" for i in range(ncols)])

        with open_method(test_file_name, 'rt') as infile:
            first_row, res_skiplines = header.get_header_from_file(
                infile, skiplines=skiplines, comment=comment_char
            )

        assert exp_header == first_row, "wrong header row"
        assert (skiplines + (bool(skiplines) * 5)) == res_skiplines, \
            "wrong number of skipped lines"
    finally:
        os.unlink(test_file_name)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "skiplines,comment_char,has_header,ncols,nrows,delimiter,open_method",
    [
        (0, None, True, 7, 10, "\t", gzip.open),
        (0, None, True, 7, 10, "\t", lzma.open),
        (10, "#", True, 7, 10, "\t", bz2.open),
        (10, "#", True, 7, 10, "\t", open),
        (0, None, True, 7, 10, "|", gzip.open),
        (0, None, True, 7, 10, "|", lzma.open),
        (10, "#", True, 7, 10, "|", bz2.open),
        (10, "#", True, 7, 10, "|", open),
        (0, None, True, 7, 10, ",", gzip.open),
        (0, None, True, 7, 10, ",", lzma.open),
        (10, "#", True, 7, 10, ",", bz2.open),
        (10, "#", True, 7, 10, ",", open),
    ]
)
def test_get_header_from_reader(tmpdir, skiplines, comment_char, has_header,
                                ncols, nrows, delimiter, open_method):
    """Tests for the header detection function
    ``pyaddons.flat_files.header.detect_header``
    """
    test_file_name = utils.get_temp_file(dir=tmpdir)

    try:
        create_file(
            test_file_name, skiplines=skiplines,
            comment_char=comment_char, has_header=has_header,
            ncols=ncols, nrows=nrows, delimiter=delimiter,
            write_method=open_method
        )

        # This is the expected header we are testing against
        exp_header = [f"column{i}" for i in range(ncols)]

        with open_method(test_file_name, 'rt') as infile:
            reader = csv.reader(infile, delimiter=delimiter)
            first_row, res_skiplines = header.get_header_from_reader(
                reader, skiplines=skiplines, comment=comment_char
            )

        assert exp_header == first_row, "wrong header row"
        assert (skiplines + (bool(skiplines) * 5)) == res_skiplines, \
            "wrong number of skipped lines"
    finally:
        os.unlink(test_file_name)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "skiplines,comment_char,has_header,ncols,nrows,delimiter,open_method,test_type",
    [
        (0, None, True, 7, 10, "\t", gzip.open, None),
        (0, None, True, 7, 10, "\t", lzma.open, csv.reader),
        (10, "#", True, 7, 10, "\t", bz2.open, csv.DictReader),
        (10, "#", True, 7, 10, "\t", open, None),
        (0, None, True, 7, 10, "|", gzip.open, csv.reader),
        (0, None, True, 7, 10, "|", lzma.open, csv.DictReader),
        (10, "#", True, 7, 10, "|", bz2.open, None),
        (10, "#", True, 7, 10, "|", open, csv.reader),
        (0, None, True, 7, 10, ",", gzip.open, csv.DictReader),
        (0, None, True, 7, 10, ",", lzma.open, None),
        (10, "#", True, 7, 10, ",", bz2.open, csv.reader),
        (10, "#", True, 7, 10, ",", open, csv.DictReader),
    ]
)
def test_move_to_header(tmpdir, skiplines, comment_char, has_header,
                        ncols, nrows, delimiter, open_method, test_type):
    """Tests for the header detection function
    ``pyaddons.flat_files.header.move_to_header``
    """
    test_file_name = utils.get_temp_file(dir=tmpdir)

    try:
        create_file(
            test_file_name, skiplines=skiplines,
            comment_char=comment_char, has_header=has_header,
            ncols=ncols, nrows=nrows, delimiter=delimiter,
            write_method=open_method
        )

        with open_method(test_file_name, 'rt') as infile:
            res_skiplines = header.move_to_header(
                infile, skiplines=skiplines, comment=comment_char
            )

            if test_type == csv.reader:
                exp_header = [f"column{i}" for i in range(ncols)]
                reader = test_type(infile, delimiter=delimiter)
                header_row = next(reader)
                assert exp_header == exp_header, "wrong reader header row"
            elif test_type == csv.DictReader:
                exp_header = [f"column{i}" for i in range(ncols)]
                reader = test_type(infile, delimiter=delimiter)
                header_row = next(reader)
                assert exp_header == list(header_row.keys()), \
                    "wrong DictReader header row"
            else:
                exp_header = delimiter.join(
                    [f"column{i}" for i in range(ncols)]
                )
                header_row = next(infile).strip()
                assert header_row == exp_header, "wrong header row"
            assert (skiplines + (bool(skiplines) * 5)) == res_skiplines, \
                "wrong number of skipped lines"

    finally:
        os.unlink(test_file_name)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "skiplines,comment_char,has_header,ncols,nrows,delimiter,open_method",
    [
        (0, None, True, 7, 10, "\t", gzip.open),
        (0, None, True, 7, 10, "\t", lzma.open),
        (10, "#", True, 7, 10, "\t", bz2.open),
        (10, "#", True, 7, 10, "\t", open),
        (0, None, True, 7, 10, "|", gzip.open),
        (0, None, True, 7, 10, "|", lzma.open),
        (10, "#", True, 7, 10, "|", bz2.open),
        (10, "#", True, 7, 10, "|", open),
        (0, None, True, 7, 10, ",", gzip.open),
        (0, None, True, 7, 10, ",", lzma.open),
        (10, "#", True, 7, 10, ",", bz2.open),
        (10, "#", True, 7, 10, ",", open),
    ]
)
def test_skip_to_header(tmpdir, skiplines, comment_char, has_header,
                               ncols, nrows, delimiter, open_method):
    """Tests for the header detection function
    ``pyaddons.flat_files.header.skip_to_header``
    """
    test_file_name = utils.get_temp_file(dir=tmpdir)

    try:
        comment_lines = 5
        create_file(
            test_file_name, skiplines=skiplines, comment_lines=comment_lines,
            comment_char=comment_char, has_header=has_header,
            ncols=ncols, nrows=nrows, delimiter=delimiter,
            write_method=open_method
        )

        with open_method(test_file_name, 'rt') as infile:
            res_skiplines = header.skip_to_header(
                infile, skiplines=skiplines, comment=comment_char,
            )

            exp_skip = []
            for i in range(skiplines):
                exp_skip.append(f"{i}")

            if comment_char is not None:
                for i in range(comment_lines):
                    exp_skip.append(f"{comment_char} {i}")

            assert exp_skip == res_skiplines, \
                "wrong skipped lines"

    finally:
        os.unlink(test_file_name)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "test_char,exp_res,allow_none,is_error",
    [
        (' ###  ', '###  ', False, False),
        (' ###  ', '###  ', True, False),
        (None, None, True, False),
        (None, None, False, True),
        ('  ', None, True, True),
        ('', None, True, True),
        ('  ', None, False, True),
        ('', None, False, True),
    ]
)
def test_check_comment_char(test_char, exp_res, allow_none, is_error):
    """Tests that ``pyaddons.flat_files.header.check_comment_char`` is working
    as expected, both with errors and valid input.
    """
    if is_error is True:
        with pytest.raises(ValueError) as the_error:
            header.check_comment_char(test_char, allow_none=allow_none)
        assert the_error.match(f"Bad comment character: '{test_char}'")
    else:
        res = header.check_comment_char(test_char, allow_none=allow_none)
        assert res == exp_res, "wrong result"
