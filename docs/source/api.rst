================
``pyaddons`` API
================

.. toctree::
   :maxdepth: 4

   api/pyaddons
   api/flat_files
   api/llm

