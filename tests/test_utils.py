"""Tests for the ``pyaddons.utils`` module
"""
from pyaddons import utils
import pytest


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "test_number,exp_result",
    [
        (1, "One"), (10, "Ten"), (99, "NintyNine"),
        (1034, "OneThousandAndThirtyFour"),
        (15876, "FifteenThousandEightHundredAndSeventySix"),
        (100048, "OneHundredThousandAndFortyEight"),
        (590752811,
         ("FiveHundredAndNintyMillionSevenHundredAndFiftyTwoThousandEight"
          "HundredAndEleven")),
        (3250000000, "ThreeBillionTwoHundredAndFiftyMillion"),
    ]
)
def test_text_numbers(test_number, exp_result):
    """Test that the number to text function is working as expected.
    """
    assert utils.get_text_number(test_number) == exp_result
