#!/bin/bash
echo "*** column-cat example ***"
echo "header: a.txt"
header a.txt
echo "header: b.txt"
header b.txt
echo "header: c.txt"
header c.txt
echo "header: d.txt"
header d.txt
# concatinate all of them
cat-column *.txt | peep
echo "header: *.txt"
cat-column *.txt | header

# Take input from STDIN
cat d.txt | cat-column - c.txt b.txt a.txt | peep
echo "header: *.txt (STDIN)"
cat d.txt | cat-column - c.txt b.txt a.txt | header
