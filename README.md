# Getting Started with pyaddons

__version__: `0.4.0a0`

Misc additional utilities used on many of my scripts and repositories, individually not big enough to warrant a repository so all lumped together.

There is [online](https://cfinan.gitlab.io/pyaddons/index.html) documentation for pyaddons.

## Installation instructions

### pip install
At present, no packages exist yet on PyPy. Therefore, for pip install you should clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/pyaddons.git
cd pyaddons
```

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then run (or add `-e` for developer install):
```
python -m pip install .
```

### Installation using conda
A conda build is also available for Python v3.8/v3.9/v3.10 on linux-64/osX-64. Although please note that all development and testing is performed in linux-64 (Python v3.9).
```
conda install -c conda-forge -c cfin -c bioconda pyaddons
```

If you are using conda and require anything different then please install with pip and install the dependencies with the environments specified in `resources/conda/env/py3*`.

### Installing companion bash scripts
In addition to the Python command-line scripts that are available when the package is installed. There are also some bash scripts located in ``./resources/bin``. Please note these will not be installed when you install via clone & pip or a conda install. If using conda you will have to clone the repo. With either install method you will need to add the ``./resources/bin`` directory to your PATH.

These scripts will require two bash libraries to be in your PATH.

1. ``shflags`` - `This <https://github.com/kward/shflags>`_ is to manage bash command line arguments.
2. ``bash-helpers`` - `This <https://gitlab.com/cfinan/bash-helpers>`_ wraps some handle bash functions.

For more information on what is available see the [bash script](https://cfinan.gitlab.io/pyaddons/scripts/bin_scripts.html) documentation.

If you are using the conda build script you will also need boa installed. If you have installed pyaddons, via conda it should already be installed. However, I can't find it in pip so it is not in `requirements.txt`,  although, if you are using pip I guess you are not worried about building conda packages.

## Change log

### version `0.1.10a0`
* SCRIPTS - Moved `column-info` and  `orm-doc-*` scripts to [sqlalchemy-config](https://gitlab.com/cfinan/sqlalchemy-config), adjusted documentation.
* SCRIPTS - Moved bash script ``./resources/bin/init-package.sh`` to the [skeleton-package](https://gitlab.com/cfinan/skeleton-package)
* SCRIPTS - ``mdtable`` and ``grpcc`` can now handle very wide delimited files.
* SCRIPTS - ``import-requirements`` is a bit more robust to different scenarios.
* API - Added a generic __repr__ function to ``pyaddons.utils``
* TESTS - added import tests
* BUILD - updated requirements/conda build to account for related scripts
* BUILD - added test imports to conda build scripts
* BUILD - fixed wrong branch for stdopen in requirements.txt

### version `0.1.11a0`
* API - ``pyaddons.flat_files.header`` added functions for identifying/handling duplicated column in file headers.
* BUILD - Removed the biopython requirement as ``get_open_method`` now always returns ``gzip.open``.

### version `0.2.0a0`
* API - ``pyaddons.flat_files.header`` added functions for checking comment characters and for returning commented/fixed skiplines.
* TESTS - Tests for functions implemented in API changes.

### version `0.3.0a0`
* API - ``pyaddons.llm.claude`` added classes for interactive with the Anthropic API.

### version `0.4.0a0`
* API - ``pyaddons.flat_files.header.skip_to_header`` added options to use input from STDIN.
