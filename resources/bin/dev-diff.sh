#!/bin/bash
root_path="${1:-$PWD}"
root_path=$(readlink -f "$root_path")

# The root path is a git repo
if [[ -e ${root_path}/.git ]]; then
	echo ""
	echo "*** Found git repo: ${root_path} ***"
	dev=$(git branch | grep dev | sed 's/\*\s*//')
	if [[ $dev == 'dev' ]]; then
		echo ""
		echo "=== Dev difference $root_path ==="
		echo ""
 		git diff --name-only dev origin/master
 	fi
else
	# The dirs below the root are repos
	for i in ${root_path}/*; do
		if [[ -e ${i}/.git ]]; then
			echo ""
			echo "*** Found git repo: ${i} ***"
			cd "$i"
			dev=$(git branch | grep dev | sed 's/\*\s*//')
			if [[ $dev == 'dev' ]]; then
				echo "=== Dev difference $i ==="
				echo ""
 				git diff --name-only dev origin/master
 			fi
 			cd ..
 		fi
	done
fi
