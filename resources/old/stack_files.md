## Overview of `stack_files` 
This is a module that is designed to make "catting" files a bit more robust. A bunch of files are stacked top to bottom to form a single file. This acts
like `cat` but is slower and also performs more checks. The files should be tabular with a header. if no delimiter is specified then it is determined with `csv.Sniffer`. If all the files do have the same delimiter then a fixed delimiter can be supplied. Under default conditions it is assumed that the headers are identical, however, there is an option to group certain column names into a proxy column that will contain their data in the output.

It will also produce a merge log file and there are options to leave the files being merged intact or to have them removed during merging, either as the merge is being done (unsafe mode) or after the merge has been completed.


It is available as a script, aliased to the names `pycat` or `stacker` or can be imported as a module and the main class `FileStack` can be used directly or inherited from.

### Examples
```
stacker --regexp 'MetaAnalysis_MAF0.001_Rsq0.3_INT_(?P<A>.+)_ALL_CHR_imputed_unadj_METAL_top_hits.txt.gz$' -v ~/Scratch/ucleb_kett_alspac_metabolomics/```
