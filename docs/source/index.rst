.. pyaddons documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyaddons
===================

The `pyaddons <https://gitlab.com/cfinan/pyaddons>`_ repository is a miscellaneous collection of scripts and utilities that are used throughout my projects. They are here as they are not big enough to be their own repository but I want a separate proper location where I can maintain them.

If a sub component within pyaddons gets too big or complex it will be deprecated and rolled into it's own repository.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
