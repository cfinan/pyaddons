"""
A helper modult for "generic" printing of output from objects
"""
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def obj_repr(obj, attr=None):
    """
    A generalised function for creating a representation string

    Parameters
    ----------

    obj : :obj:`Any`
        An object to extract attributes from
    attr :`list` of `str`
        Attributes of the object to be included in the output. If None or
        len==0, then all non-function attributes are used

    Returns
    -------

    rep :`str`
        The string representation of the object
    """

    if attr is None or len(attr) == 0:
        # Get all the non private objects, note that this will return methods
        # as well.
        # TODO: remove methods from here
        attr = [i for i in dir(obj)
                if not i.startswith('_')]
    #     pp.pprint(attr)
    # print("")

    # Filter out any method calls
    attr_values = []
    for i in attr:
        val = getattr(obj, i)
        # print(i, type(val), val.__class__.__name__)
        # if type(val) != "<class 'method'>":
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i, val))

    # pp.pprint(attr_values)
    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))

    # return obj.__class__.__name__
