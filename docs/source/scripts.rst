======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with the pyaddons.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scripts/python_scripts
   scripts/bin_scripts

