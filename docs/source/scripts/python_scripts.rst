==============
Python Scripts
==============

pyaddons implements several command line scripts. Fitting with the miscellaneous nature of the package, these cover a wide variety of areas but the main thrust is to make certain command line tasks easier.

Flat file handling
------------------

``header``
~~~~~~~~~~

.. _header:

.. argparse::
   :module: pyaddons.flat_files.header
   :func: _init_cmd_args
   :prog: header

``grpcc``
~~~~~~~~~

.. _grpcc:

.. argparse::
   :module: pyaddons.flat_files.grpcc
   :func: _init_cmd_args
   :prog: grpcc

``grep-column``
~~~~~~~~~~~~~~~

.. _grep_column:

.. argparse::
   :module: pyaddons.flat_files.column_grep
   :func: _init_cmd_args
   :prog: grep-column

``regex-column``
~~~~~~~~~~~~~~~~

.. _regex_column:

.. argparse::
   :module: pyaddons.flat_files.regex_column
   :func: _init_cmd_args
   :prog: regex-column

``cat-column``
~~~~~~~~~~~~~~

.. _cat_column:

.. argparse::
   :module: pyaddons.flat_files.column_cat
   :func: _init_cmd_args
   :prog: cat-column

``rejig``
~~~~~~~~~

.. _rejig:

.. argparse::
   :module: pyaddons.flat_files.rejig
   :func: _init_cmd_args
   :prog: rejig

``pad``
~~~~~~~

.. _pad:

.. argparse::
   :module: pyaddons.flat_files.pad
   :func: _init_cmd_args
   :prog: pad


``line-length``
~~~~~~~~~~~~~~~

.. _line-length:

.. argparse::
   :module: pyaddons.flat_files.line_len
   :func: _init_cmd_args
   :prog: line-length


Documentation building/formatting
---------------------------------

These are various scripts that I use when putting together packages/documentation. These are probably not of general interest but are included in pyaddons to keep them all together.

``doc-column-list``
~~~~~~~~~~~~~~~~~~~

.. _doc_cols:

.. argparse::
   :module: pyaddons.docs.column_list
   :func: _init_cmd_args
   :prog: doc-column-list

``import-requirements``
~~~~~~~~~~~~~~~~~~~~~~~

.. _import_req:

.. argparse::
   :module: pyaddons.docs.imports
   :func: _init_cmd_args
   :prog: import-requirements

``mdtable``
~~~~~~~~~~~

.. _mdtable:

.. argparse::
   :module: pyaddons.docs.mdtable
   :func: _init_cmd_args
   :prog: mdtable
