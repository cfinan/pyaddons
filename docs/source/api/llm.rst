``pyaddons.llm`` sub-package
============================

Miscellaneous small APIs that I have used to query LLMs, not much in here at the moment.

``pyaddons.llm.claude`` module
------------------------------

.. automodule:: pyaddons.llm.claude
   :members:
   :undoc-members:
   :show-inheritance:
