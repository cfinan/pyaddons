# Overview of `mdtable`
This module contains code that can format a markdown table, **mdtable** formats tabular data for insertion into a markdown document. This is useful for taking a small about of database output and displaying in a readme, although there is no reason why it can't process larger amounts of data. It tries to use **unicodecsv** if installed `pip install unicodecsv`. Otherwise, unicode support is disabled and the script uses the standard **csv** library. Other modules used are standard python ones: **argparse**,**sys**,**os**,**tempfile**.

## How `mdtable` works
At the moment **mdtable** only accepts input from **<stdin>**. In the default mode it expects the input to be tab-delimited (can be changed with **-d** / **--delimiter**) and any unwanted tabs to be quoted with " (double quotation mark - can be changed with **-q**/**--quote**). It will read in upto the first 1000 lines (can be changed with **-b**/**--buffer**)  and use this to determine the widths of the strings in each colun. It will then use these to pad the strings with spaces in the final markdown output. It also adds **--npad** spaces (default 1) to each side of the padded string (asthetics). It is possible that the first 1000 lines doesn't give an accurate estimation of the string widths in the whole table, obviously if this happens then the command line output will be messed up, alternatively you can specify the --cut flag and then any strings that are greater than the column width will be truncated and a "**>>**" will be placed at the end o the string. Also, other approaches can be increasing the --buffer value or if memory is a problem then the **-f**/**--file** option can be used and the input data will be written to a temp file while the string lengths are determined and then the data will be displayed by reading the tmp file back in, the temp file is then deleted upon completeion. By default the first column will be left justifed and the last column will be right justified (in both the text output and the markdown).

## Usage
Command line usage is shown below. Currently there are no real pythonic entry points
```
$ mdtable --help
usage: mdtable [-h] [-d DELIMITER] [-q QUOTE] [-b BUFFER] [-r ROUND] [-f] [-c]
               [-u] [--npad NPAD]

Takes tabular STDIN and FORMATS markdown tables in to STDOUT

optional arguments:
  -h, --help            show this help message and exit
  -d DELIMITER, --delimiter DELIMITER
                        The delimiter of STDIN
  -q QUOTE, --quote QUOTE
                        The Quoting character
  -b BUFFER, --buffer BUFFER
                        Output lines to buffer before writing a temp file/or
                        output
  -r ROUND, --round ROUND
                        Round floats to --round dp
  -f, --file            When the buffer is full write output to a temp file
                        (memory saver)
  -c, --cut             Truncate any strings that are longer then the column
                        width
  -u, --unicode         Have nice unicode table dividers
  --npad NPAD           Add --npad number of spaces to the start and end of
                        the string

```

## Examples
This uses a very small buffer so the column widths are based on the widths of the header columns and any strings exceeding that are truncated to make the output nice. This is more appropriate for the command line than markdown. Floats in this example are rounded to 4 dp.

```sh
$ sqlite3 -header /SAN/UCLEB/projects/dtadb/research/mesh_db/data/mesh_2015.db "SELECT * FROM mesh_term LIMIT 10" | mdtable -d"|" -cb1 -r4

 mesh_id | mesh_term  | term_precision 
:--------|:-----------|---------------:
 D000001 | Calcimycin |              1 
 D000002 | Temefos    |         0.2667 
 D000003 | Abattoirs  |              1 
 D000004 | Abbrevia>> |              1 
 D000005 | Abdomen    |              1 
 D000006 | Abdomen,>> |         0.4000 
 D000007 | Abdomina>> |              1 
 D000008 | Abdomina>> |              1 
 D000009 | Abdomina>> |              1 
 D000010 | Abducens>> |              1 
```

The same as above but not cutting any strings and rounding floats to 1dp

```
$ sqlite3 -header /SAN/UCLEB/projects/dtadb/research/mesh_db/data/mesh_2015.db "SELECT * FROM mesh_term LIMIT 10" | mdtable -d"|" -b1 -r1

 mesh_id | mesh_term  | term_precision 
:--------|:-----------|---------------:
 D000001 | Calcimycin |              1 
 D000002 | Temefos    |            0.3 
 D000003 | Abattoirs  |              1 
 D000004 | Abbreviations as Topic |              1 
 D000005 | Abdomen    |              1 
 D000006 | Abdomen, Acute |            0.4 
 D000007 | Abdominal Injuries |              1 
 D000008 | Abdominal Neoplasms |              1 
 D000009 | Abdominal Muscles |              1 
 D000010 | Abducens Nerve |              1 
```

Using plenty of buffer and also adding padding to the columns

```
$ sqlite3 -header /SAN/UCLEB/projects/dtadb/research/mesh_db/data/mesh_2015.db "SELECT * FROM mesh_term LIMIT 10" | mdtable -d"|" -b1000000 -r2 --npad 4

 mesh_id    |    mesh_term                 |    term_precision    
:-----------|:-----------------------------|---------------------:
 D000001    |    Calcimycin                |                 1    
 D000002    |    Temefos                   |              0.27    
 D000003    |    Abattoirs                 |                 1    
 D000004    |    Abbreviations as Topic    |                 1    
 D000005    |    Abdomen                   |                 1    
 D000006    |    Abdomen, Acute            |              0.40    
 D000007    |    Abdominal Injuries        |                 1    
 D000008    |    Abdominal Neoplasms       |                 1    
 D000009    |    Abdominal Muscles         |                 1    
 D000010    |    Abducens Nerve            |                 1   
```

Beautify the output with some unicode table dividers, not much use for markdown but nice for terminal output.

```
$ sqlite3 -header /SAN/UCLEB/projects/dtadb/research/mesh_db/data/mesh_2015.db "SELECT * FROM mesh_term LIMIT 10" | mdtable -d"|" -u

 mesh_id │ mesh_term              │ term_precision
─────────┼────────────────────────┼────────────────
 D000001 │ Calcimycin             │              1
 D000002 │ Temefos                │           0.27
 D000003 │ Abattoirs              │              1
 D000004 │ Abbreviations as Topic │              1
 D000005 │ Abdomen                │              1
 D000006 │ Abdomen, Acute         │           0.40
 D000007 │ Abdominal Injuries     │              1
 D000008 │ Abdominal Neoplasms    │              1
 D000009 │ Abdominal Muscles      │              1
 D000010 │ Abducens Nerve         │              1
```

## `peep` - using `mdtable` as a viewer
**mdtable** can be repurposed as a tabular viewer on the command line. Essentially you can pipe `mdtable` through `less -S` for very wide tables.

As this is so usefull a wrapper bash script has been created called `peep`, that is located in `pyaddons/resources/bin/peep`. This can be added to your path and used as a viewer. It accepts the `mdtable` command line arguments and can also accept input from `STDIN`  
