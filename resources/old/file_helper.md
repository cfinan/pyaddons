## Overview of **`file_helper`**
This is a miscellaneous collection of functions and classes that perform checks/manipulations with files. See the API documentation for more details.
