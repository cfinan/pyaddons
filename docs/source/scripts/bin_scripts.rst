==========================
``./resouces/bin`` scripts
==========================

The pyaddons repository also contains some other scripts in the ``./resouces/bin`` `directory <https://gitlab.com/cfinan/pyaddons/-/tree/master/resources/bin>`_. Unlike the Python scripts these are not installed into the PATH and the ``./resouces/bin`` must be added manually to your PATH in your ``~/.bashrc``.

Most of these are bash scripts and many of these scripts will have the `bash-helpers <https://gitlab.com/cfinan/bash-helpers>`_ repository as a dependency and that in turn requires `shflags <https://github.com/kward/shflags>`_ to be in your PATH.

``peep``
~~~~~~~~

.. _peep:

A tabular viewer for the terminal. This is a wrapper around :ref:`mdtable <mdtable>` and pipes it into ``less -S``.

.. code-block::

   $ peep --help
   === peep v0.3 ===
   usage: peep options [infile]

   [infile] optional input file to view, can also use STDIN

   Display formatted contents of a file. This is a wrapper around mdtable and
   pipes it into less -S. The options are the options to mdtable, although peep
   has the -u and --all-left options flagged already:
   usage: mdtable [-h] [-d DELIMITER] [-q QUOTE] [-b BUFFER] [-r ROUND] [-f] [-c] [-u] [--all-left] [--npad NPAD]

   Takes tabular STDIN and FORMATS markdown tables in to STDOUT

   optional arguments:
     -h, --help            show this help message and exit
     -d DELIMITER, --delimiter DELIMITER
                           The delimiter of STDIN
     -q QUOTE, --quote QUOTE
                           The Quoting character
     -b BUFFER, --buffer BUFFER
                           Output lines to buffer before writing a temp file/or output
     -r ROUND, --round ROUND
                           Round floats to --round dp
     -f, --file            When the buffer is full write output to a temp file (memory saver)
     -c, --cut             Truncate any strings that are longer then the column width
     -u, --unicode         Have nice unicode table dividers
     --all-left            Do not right justify the final column.
     --npad NPAD           Add --npad number of spaces to the start and end of the string


``inc-version.sh``
~~~~~~~~~~~~~~~~~~

.. _inc_version:

A wrapper around `bump2version <https://pypi.org/project/bump2version/>`_. This basically runs a dry run and asks the user if they want to continue before running the full command. This assumes that the directory structure is the same as that in the `skeleton-package <https://gitlab.com/cfinan/skeleton-package>`_.

.. code-block::

   $ inc_version.sh --help
   === increment-version ===
   Bump the version number. In most cases <version> should be
   major, minor, patch or release, although this may depend on the
   .bumpversion.cfg file.

   USAGE: inc_version.sh [flags] <repo_root> <version>
   flags:
     -h,--help:  show this help (default: false)


``build-docs.sh``
~~~~~~~~~~~~~~~~~

.. _build_docs:

Build Sphinx documentation that resides within a repository root directory. This assumes that the directory structure is the same as that in the `skeleton-package <https://gitlab.com/cfinan/skeleton-package>`_. That is that make file is located in ``./docs/make`` and there is a ``./resources`` directory. It also copies the ``README.md`` file in the root into a ``./docs/source/getting_started.md`` file and alters the paths of any HTML referenced images that may be present to match the location in the ``./docs/build/html`` directory.

Please note that this is not really maintained anymore as I am moving over to using GitLab CI/CD files.

.. code-block:: console

   $ build_docs.sh --help
   === build-docs ===
   Run the set of Sphinx commands to build both HTML and PDF documentation
    and then copy the PDF out of the build directory to ./resources/pdf

   USAGE: build_docs.sh [flags] <repo_root>
   flags:
     -c,--[no]clean:  Perform a make clean before building the documentation (default: false)
     -h,--help:  show this help (default: false)


``build-conda.sh``
~~~~~~~~~~~~~~~~~~

.. _build_conda:

Build conda packages for specific Python versions or all available versions. This will only build for the current system and osx-64.

For this, you will need boa installed. If you have installed pyaddons, via conda it should already be installed. However, I can't find it in pip so it is not in ``requirements.txt``,  although, if you are using pip I guess you are not worried about building conda packages.

.. code-block::

   USAGE: build-conda.sh [flags] <repo_root>
   flags:
     -v,--version:  The version to build against. i.e. py38, this will be a name in your repo directory <repo_root>/resources/conda/build/<version>
                    (default: 'all')
     -h,--help:  show this help (default: false)


``cct``
~~~~~~~

.. _cct:

This counts the minimum number/maximum number of columns in a text file and the number of rows. It is written in C and this pre-compiled binary is designed to work on 64 systems. However, please note it is not aware of protected fields.

It can take input from STDIN or an uncompressed input file. The output is Example usage is shown below:

.. code-block::

   # default delimiter is a comma, to use on a tab-delimited file
   # Output is min_cols,max_cols,nrows
   $ zcat infile.txt.gz | cct -d$'\t'
   10,10,6264

   $ cct -d$'\t' infile.txt
   10,10,6264 - infile.txt


``gitstatus``
~~~~~~~~~~~~~

.. _gitstatus:

Run ``git fetch`` / ``git status`` commands on git repositories that are within a central code directory. This enables you to see all the repositories that are not up-to-date or have changes. The central code directory is determined by a ``CODE_DIR`` environment variable that should be defined in your ``~/.bashrc``.

.. code-block::

   $ gitstatus
   ***************
   ** code-repo **
   ***************
   Fetching origin
     origin/master
   * master
   On branch master
   Your branch is behind 'origin/master' by 5 commits, and can be fast-forwarded.
     (use "git pull" to update your local branch)

   Untracked files:
     (use "git add <file>..." to include in what will be committed)
     lib/library.sh

``dev-diff.sh``
~~~~~~~~~~~~~~~

.. _devdiff:

This runs a name only git diff between the master branch in origin and any branch called ``dev``. This gives an indication if the dev is different from master. It takes one argument, the directory to test. If that directory is a git repo it will be tested, if not then all directories below it will be tested.

The example below lists the differences between the ``dev`` branch and the pyaddons ``origin/master`` branches. It lists one file that is different.

.. code-block::

   $ dev-diff.sh .
   *** Found git repo: /home/code/pyaddons ***
   === Dev difference /home/code/pyaddons ===
   resources/bin/dev-diff.sh
