# Some common functions for the admin scripts
check_repo() {
  # Make sure the expected directories and files are found in the repo
  # this will also initialise paths that are used
  local root_dir="$1"

  # dirs
  git_dir="$root_dir"/.git
  resources_dir="$root_dir"/resources
  images_dir="$resources_dir"/images
  build_dir="$resources_dir"/conda/build
  conda_dir="$resources_dir"/conda
  sphinx_docs="$root_dir"/docs
  sphinx_source="$sphinx_docs"/source
  # sphinx_build="$sphinx_docs"/build
  # sphinx_static="$sphinx_source"/_static

  # files
  ignore_file="$root_dir"/.gitignore
  glci_file="$root_dir"/.gitlab-ci.yml
  version="$root_dir"/.bumpversion.cfg
  main_readme="$root_dir"/README.md
  sphinx_conf="$sphinx_source"/conf.py
  sphinx_make="$sphinx_docs"/Makefile

  dirs=(
      "$git_dir"
      "$resources_dir"
      "$images_dir"
      "$build_dir"
      "$conda_dir"
      "$sphinx_source"
      # "$sphinx_static"
  )
  files=(
      "$ignore_file"
      "$glci_file"
      "$version"
      "$main_readme"
      "$sphinx_conf"
      "$sphinx_make"
  )

  for i in "${dirs[@]}"; do
    if [[ -d "$i" ]]; then
      echo "[info] found directory $i"
    else
      echo "[error] directory not found $i" 2>&1
      return 1
    fi
  done

  for i in "${files[@]}"; do
    if [[ -e "$i" ]]; then
      echo "[info] found file $i"
    else
      echo "[error] file not found $i" 2>&1
      return 1
    fi
  done
}
