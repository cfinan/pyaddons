``pyaddons.flat_files`` sub-package
===================================

``pyaddons.flat_files.header`` module
-------------------------------------

.. currentmodule:: pyaddons.flat_files.header

.. autofunction:: get_header_from_file
.. autofunction:: get_header_from_reader
.. autofunction:: move_to_header
.. autofunction:: skip_to_header
.. autofunction:: check_comment_char
.. autofunction:: detect_header
.. autofunction:: make_unique_header
.. autofunction:: compare_unique_header
.. autofunction:: duplicated_columns


``pyaddons.flat_files.line_len`` module
---------------------------------------

.. currentmodule:: pyaddons.flat_files.line_len

.. autofunction:: line_length
