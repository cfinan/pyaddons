#!/bin/bash
# Helper script to build conda packages

# Run the abort sequence
abort() {
    echo "[info] aborting..." 1>&2
    cd "$curdir"

    if [[ -e "$tmp_dir" ]]; then
        rm -r "$tmp_dir"
    fi
    exit 1
}

# run the conda build process
build() {
    local build_for="$1"
    echo "[info] **** starting build for: $build_for ****"
    # mamba install -cconda-forge boa
    conda mambabuild  -c "cfin"  -c "bioconda" -c "conda-forge" "$build_for"

    # Don'y use mamba build for this
    built_file=$(conda build "$build_for" --output)

    if [[ ! -e "$built_file" ]]; then
        echo "[error] file does not exist: $built_file" 1>&2
        abort
    fi
    echo "[info] **** uploading: $built_file ****"
    anaconda upload "$built_file"
    echo "[info] **** converting: $built_file ****"
    conda convert --platform "osx-64" -o "$tmp_dir" "$built_file"
    anaconda upload "$tmp_dir"/osx-64/*.tar.bz2
}


PROG_NAME="=== build-conda.sh ==="
USAGE=$(
cat <<EOF
$PROG_NAME
Build conda packages for specific Python versions or all available versions. This will only build for the current system and osx-64.

USAGE: $(basename $0) [flags] <repo_root>
EOF
)

. shflags

DEFINE_string 'version' "all" 'The version to build against. i.e. py38, this will be a name in your repo directory <repo_root>/resources/conda/build/<version>' 'v'

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Make sure the root directory is set
if [[ -z "$1" ]]; then
  echo "[error] no repo root defined" 2>&1
  exit 1
fi

# Get user login details
login_info=$(anaconda whoami 2>&1)

# This will be empty if logged in (as long as the user is not called ananymous)
login_valid=$(echo "$login_info" | grep -i "Username:")

if [[ "$login_valid" == "" ]]; then
    GO="z"
    while [[ "$GO" != 'y' ]] && [[ "$GO" != 'n' ]]; do
        # Now ask the user if they want to login to anaconda
        echo "[message] You might not be logged in to anaconda. Do you want to login (y/n)? "
        read GO
    done
fi

if [[ "$GO" == "y" ]]; then
    anaconda login
fi

# exit on error and undef not allowed
set -eu

# source in some common functions
run_dir="$(dirname $0)"
. "$run_dir"/common.sh

ROOT_DIR=$(readlink -f "$1")

echo "$PROG_NAME"
echo "[info] repo root: $ROOT_DIR"
echo "[info] version: $FLAGS_version"

# Make sure all the expected files and dirs are available
# **** this also sets a load of variables that will be used below ****
check_repo "$ROOT_DIR"

curdir="$PWD"

trap 'abort' SIGINT ERR

# create a tmpdir to build additional versions
tmp_dir=$(mktemp -d)

versions=("$build_dir"/py*)
for i in "${versions[@]}"; do
    echo "[info] found build version: $i"
done

# If we want to build against a specific version
if [[ "$FLAGS_version" != "all" ]]; then
    found=0
    for i in "${versions[@]}"; do
        version_name="$(basename "$i")"
        if [[ "$version_name" == "$FLAGS_version" ]]; then
            echo "[info] building version: $i"
            found=1
            build "$i"
        fi
    done

    if [[ $found -eq 0 ]]; then
        echo "[error] can't find build for version: $FLAGS_version" 1>&2
        abort
    fi
else
    for i in "${versions[@]}"; do
        version_name="$(basename "$i")"
        echo "[info] building version: $i"
        build "$i"
    done
fi

cd "$curdir"
echo "*** END ***"
