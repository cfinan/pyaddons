"""
Misc checking functions, these will usually check a value is ok and throw an
error if not.
"""
import re


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_email(email):
    """
    A very basic checker of the E-mail address. See here for the complexities:
    https://stackoverflow.com/questions/8022530/how-to-check-for-valid-email-address

    Parameters
    ----------

    email :`str`
        The E-mail address supplied in the config file

    Raises
    ------

    ValueError
        If the email fails the regexp test
    """

    # A very basic regex taken from the link above
    if not re.fullmatch(r'[^@]+@[^@]+\.[^@]+', email):
        raise ValueError("does not look like an e-mail address: '{0}'".format(
            email))
