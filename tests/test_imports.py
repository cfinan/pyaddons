import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'pyaddons',
        'pyaddons.log',
        'pyaddons.utils',
        'pyaddons.flat_files.column_cat',
        'pyaddons.flat_files.column_grep',
        'pyaddons.flat_files.grpcc',
        'pyaddons.flat_files.header',
        'pyaddons.flat_files.line_len',
        'pyaddons.flat_files.pad',
        'pyaddons.flat_files.regex_column',
        'pyaddons.flat_files.rejig',
        'pyaddons.docs.column_list',
        'pyaddons.docs.imports',
        'pyaddons.docs.mdtable',
        'pyaddons.llm.claude',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
